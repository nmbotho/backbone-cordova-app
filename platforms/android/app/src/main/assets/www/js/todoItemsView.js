var TodoItemsView = Backbone.View.extend({

    id: 'todoItemsContainer',

    initialize: function(options) {
        if (!(options) || !options.model)
            throw new Error('model is not specified.');

        //listen to add event of the collection
        this.model.on('add', this.onAddTodoItem, this);
        this.model.on('remove', this.onDeleteItem, this);
    },

    events: {
        'keypress #newItemInput': 'onKeyPress' 
    },

    onKeyPress: function(e) {
        if (e.keyCode == 13) {
            var $inputElem = this.$('#newItemInput');

            if ($inputElem.val()) {
                var todoItem = new TodoItem({ title: $inputElem.val() });

                this.model.create(todoItem);

                /*  
                    THE ABOVE IS EQUIVALENT TO THIS

                    this.model.add(todoItem);
                    this.model.save();  
                */

                $inputElem.val("");
            }
        }
    },

    onAddTodoItem: function(todoItem) {
        var view = new TodoItemView({ model: todoItem });
        this.$('#todoItemsView').append(view.render().$el);
    },

    onClickAdd: function() {
        
    },

    onDeleteItem: function(item) {
        this.$('li#' + item.get('id')).remove();
    },

    render: function() {
        var self = this;

        var template = $('#todoItemsTemplate').html();

        var html = Mustache.render(template);
        this.$el.html(html);

        return this;
    }
});