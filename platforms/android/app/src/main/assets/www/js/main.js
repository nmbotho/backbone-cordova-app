$(document).ready(function() {

    var todoItems = new TodoItems();

    todoItems.fetch();

    var listView = new TodoItemsView({
        model: todoItems
    });

    $('body').append(listView.render().$el);
});